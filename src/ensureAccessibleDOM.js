(function () {

var fields = document.querySelectorAll('form p');

[].forEach.call(fields, function (field) { 
    var required = field.querySelector('span');
    var label = field.querySelector('label');
    var input = field.querySelector('input');
    var labelFor;
    if (required && label) {
        label.appendChild(required);
    }
    if (label && input && !(labelFor = label.getAttribute('for'))) {
        label.setAttribute('for', input.id);
    }
    if (input && input.tabIndex > 0) {
        input.removeAttribute('tabindex');
    }
});

}());
