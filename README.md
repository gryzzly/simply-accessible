To install dependencies run:
```
npm install
```
After dependencies are installed, to build minified JS bundle run:
```
npm run build
```

To lint JS files in `/src` dir on file save:
```
npm run watch
```

Note: In real life I'd add a development server to serve updated bundle and 
introduce dev/production environments to didstuinguish between the two different 
build targets.
